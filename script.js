// MAIN ----- our-services ------
let tabs = document.querySelectorAll('.tabs-title');
let tabContent = document.querySelectorAll('.tabs-content li');

tabs.forEach(tab => {
    tab.addEventListener('click', activeTab)
})

function activeTab(event){
    tabs.forEach(tab => {
    if (tab === event.target) {
        tab.classList.add('active');
        tabContent.forEach(cont => {
           if(cont.getAttribute('data-name') == tab.getAttribute('data-name')) {
            cont.classList.add('active'); 
            let dataValue = cont.getAttribute('data-name');
            let contentFoto = document.querySelector('.content.active img');
            contentFoto.setAttribute('src', `img/3.ourServices/${dataValue}.jpg`)
           }
           else {
            cont.classList.remove('active');
           }
        });

    } else {
        tab.classList.remove('active');
    } 
});
};


// MAIN ----- our-amazing-work ------

let graphicDesign = [
    {
    name: "Graphic Design", 
    text: "CREATIVE DESIGN text1",
    img: 'img/5.ourAmazingWork/graphic-design/graphic-design1.jpg'
   },
   {
    name: "Graphic Design", 
    text: "CREATIVE DESIGN text2",
    img: 'img/5.ourAmazingWork/graphic-design/graphic-design2.jpg'
   },
   {
    name: "Graphic Design", 
    text: "CREATIVE DESIGN text3",
    img: 'img/5.ourAmazingWork/graphic-design/graphic-design3.jpg'
   },
   {
    name: "Graphic Design", 
    text: "CREATIVE DESIGN text4",
    img: 'img/5.ourAmazingWork/graphic-design/graphic-design4.jpg'
   },
   {
    name: "Graphic Design", 
    text: "CREATIVE DESIGN text5",
    img: 'img/5.ourAmazingWork/graphic-design/graphic-design5.jpg'
   },
   {
    name: "Graphic Design", 
    text: "CREATIVE DESIGN text6",
    img: 'img/5.ourAmazingWork/graphic-design/graphic-design6.jpg'
   },
   {
    name: "Graphic Design", 
    text: "CREATIVE DESIGN text7",
    img: 'img/5.ourAmazingWork/graphic-design/graphic-design7.jpg'
   },
   {
    name: "Graphic Design", 
    text: "CREATIVE DESIGN text8",
    img: 'img/5.ourAmazingWork/graphic-design/graphic-design8.jpg'
   },
   {
    name: "Graphic Design", 
    text: "CREATIVE DESIGN text9",
    img: 'img/5.ourAmazingWork/graphic-design/graphic-design9.jpg'
   },
   {
    name: "Graphic Design", 
    text: "CREATIVE DESIGN text10",
    img: 'img/5.ourAmazingWork/graphic-design/graphic-design10.jpg'
   },
   {
    name: "Graphic Design", 
    text: "CREATIVE DESIGN text11",
    img: 'img/5.ourAmazingWork/graphic-design/graphic-design11.jpg'
   },
   {
    name: "Graphic Design", 
    text: "CREATIVE DESIGN text12",
    img: 'img/5.ourAmazingWork/graphic-design/graphic-design12.jpg'
   },


   {
    name: "Graphic Design", 
    text: "CREATIVE DESIGN text1",
    img: 'img/5.ourAmazingWork/graphic-design/graphic-design1.jpg'
   },
   {
    name: "Graphic Design", 
    text: "CREATIVE DESIGN text2",
    img: 'img/5.ourAmazingWork/graphic-design/graphic-design2.jpg'
   },
   {
    name: "Graphic Design", 
    text: "CREATIVE DESIGN text3",
    img: 'img/5.ourAmazingWork/graphic-design/graphic-design3.jpg'
   },
   {
    name: "Graphic Design", 
    text: "CREATIVE DESIGN text4",
    img: 'img/5.ourAmazingWork/graphic-design/graphic-design4.jpg'
   },
   {
    name: "Graphic Design", 
    text: "CREATIVE DESIGN text5",
    img: 'img/5.ourAmazingWork/graphic-design/graphic-design5.jpg'
   },
   {
    name: "Graphic Design", 
    text: "CREATIVE DESIGN text6",
    img: 'img/5.ourAmazingWork/graphic-design/graphic-design6.jpg'
   },
   {
    name: "Graphic Design", 
    text: "CREATIVE DESIGN text7",
    img: 'img/5.ourAmazingWork/graphic-design/graphic-design7.jpg'
   },
   {
    name: "Graphic Design", 
    text: "CREATIVE DESIGN text8",
    img: 'img/5.ourAmazingWork/graphic-design/graphic-design8.jpg'
   },
   {
    name: "Graphic Design", 
    text: "CREATIVE DESIGN text9",
    img: 'img/5.ourAmazingWork/graphic-design/graphic-design9.jpg'
   },
   {
    name: "Graphic Design", 
    text: "CREATIVE DESIGN text10",
    img: 'img/5.ourAmazingWork/graphic-design/graphic-design10.jpg'
   },
   {
    name: "Graphic Design", 
    text: "CREATIVE DESIGN text11",
    img: 'img/5.ourAmazingWork/graphic-design/graphic-design11.jpg'
   },
   {
    name: "Graphic Design", 
    text: "CREATIVE DESIGN text12",
    img: 'img/5.ourAmazingWork/graphic-design/graphic-design12.jpg'
   }
]

let webDesign = [
   {
    name: "Web Design", 
    text: "Web Design text1",
    img: 'img/5.ourAmazingWork/web-design/web-design1.jpg'
   },
   {
    name: "Web Design", 
    text: "Web Design text2",
    img: 'img/5.ourAmazingWork/web-design/web-design2.jpg'
   },
   {
    name: "Web Design", 
    text: "Web Design text3",
    img: 'img/5.ourAmazingWork/web-design/web-design3.jpg'
   },
   {
    name: "Web Design", 
    text: "Web Design text4",
    img: 'img/5.ourAmazingWork/web-design/web-design4.jpg'
   },
   {
    name: "Web Design", 
    text: "Web Design text5",
    img: 'img/5.ourAmazingWork/web-design/web-design5.jpg'
   },
   {
    name: "Web Design", 
    text: "Web Design text6",
    img: 'img/5.ourAmazingWork/web-design/web-design6.jpg'
   },
   {
    name: "Web Design", 
    text: "Web Design text7",
    img: 'img/5.ourAmazingWork/web-design/web-design7.jpg'
   }
]

let landingPage = [
    {
     name: "Landing Pages", 
     text: "Landing Pages text1",
     img: 'img/5.ourAmazingWork/landing-page/landing-page1.jpg'
    },
    {
     name: "Landing Pages", 
     text: "Landing Pages text2",
     img: 'img/5.ourAmazingWork/landing-page/landing-page2.jpg'
    },
    {
     name: "Landing Pages", 
     text: "Landing Pages text3",
     img: 'img/5.ourAmazingWork/landing-page/landing-page3.jpg'
    },
    {
     name: "Landing Pages", 
     text: "Landing Pages text4",
     img: 'img/5.ourAmazingWork/landing-page/landing-page4.jpg'
    },
    {
     name: "Landing Pages", 
     text: "Landing Pages text5",
     img: 'img/5.ourAmazingWork/landing-page/landing-page5.jpg'
    },
    {
     name: "Landing Pages", 
     text: "Landing Pages text6",
     img: 'img/5.ourAmazingWork/landing-page/landing-page6.jpg'
    },
    {
     name: "Landing Pages", 
     text: "Landing Pages text7",
     img: 'img/5.ourAmazingWork/landing-page/landing-page7.jpg'
    },

    {
        name: "Landing Pages", 
        text: "Landing Pages text1",
        img: 'img/5.ourAmazingWork/landing-page/landing-page1.jpg'
       },
       {
        name: "Landing Pages", 
        text: "Landing Pages text2",
        img: 'img/5.ourAmazingWork/landing-page/landing-page2.jpg'
       },
       {
        name: "Landing Pages", 
        text: "Landing Pages text3",
        img: 'img/5.ourAmazingWork/landing-page/landing-page3.jpg'
       },
       {
        name: "Landing Pages", 
        text: "Landing Pages text4",
        img: 'img/5.ourAmazingWork/landing-page/landing-page4.jpg'
       },
       {
        name: "Landing Pages", 
        text: "Landing Pages text5",
        img: 'img/5.ourAmazingWork/landing-page/landing-page5.jpg'
       },
       {
        name: "Landing Pages", 
        text: "Landing Pages text6",
        img: 'img/5.ourAmazingWork/landing-page/landing-page6.jpg'
       },
       {
        name: "Landing Pages", 
        text: "Landing Pages text7",
        img: 'img/5.ourAmazingWork/landing-page/landing-page7.jpg'
       }
       
]

let wordpress = [
    {
    name: "Wordpress", 
    text: "Wordpress text1",
    img: 'img/5.ourAmazingWork/wordpress/wordpress1.jpg'
    },
    {
    name: "Wordpress", 
    text: "Wordpress text2",
    img: 'img/5.ourAmazingWork/wordpress/wordpress2.jpg'
    },
    {
    name: "Wordpress", 
    text: "Wordpress text3",
    img: 'img/5.ourAmazingWork/wordpress/wordpress3.jpg'
    },
    {
    name: "Wordpress", 
    text: "Wordpress text4",
    img: 'img/5.ourAmazingWork/wordpress/wordpress4.jpg'
    },
    {
    name: "Wordpress", 
    text: "Wordpress text5",
    img: 'img/5.ourAmazingWork/wordpress/wordpress5.jpg'
    },
    {
    name: "Wordpress", 
    text: "Wordpress text6",
    img: 'img/5.ourAmazingWork/wordpress/wordpress6.jpg'
    },
    {
    name: "Wordpress", 
    text: "Wordpress text7",
    img: 'img/5.ourAmazingWork/wordpress/wordpress7.jpg'
    },
    {
    name: "Wordpress", 
    text: "Wordpress text8",
    img: 'img/5.ourAmazingWork/wordpress/wordpress8.jpg'
    },
    {
    name: "Wordpress", 
    text: "Wordpress text9",
    img: 'img/5.ourAmazingWork/wordpress/wordpress9.jpg'
    },
    {
    name: "Wordpress", 
    text: "Wordpress text10",
    img: 'img/5.ourAmazingWork/wordpress/wordpress10.jpg'
    }
]

let all = [...graphicDesign.slice(0,3),...webDesign.slice(0,3),...landingPage.slice(0,3),...wordpress.slice(0,3),
...graphicDesign.slice(9,12),...webDesign.slice(4,7),...landingPage.slice(4,7),...wordpress.slice(5,8),
]

let tabsFilter = document.querySelectorAll('.our-amazing-tab');
let photoBlocks = document.querySelector('.our-amazing-foto');
let cards; 

arrToCards (all);
cards = document.querySelectorAll('.foto-conteiner');
hideSomeCards ();

tabsFilter.forEach(tabFilter => 
    tabFilter.addEventListener('click', showOurWork)
)

function showOurWork () {
    let dataFilt = this.getAttribute('data-filt');

    if (dataFilt==='graphicDesign') {
        cardsActions (graphicDesign);
    } else if (dataFilt==='webDesign') {
        cardsActions (webDesign);
    } else if (dataFilt==='landingPage') {
        cardsActions (landingPage);
    } else if (dataFilt==='wordpress') {
        cardsActions (wordpress);
    } else if (dataFilt==='all') {
        cardsActions (all);
    }
}

function cardsActions (nameOfArr) {
cards = document.querySelectorAll('.foto-conteiner');
delleteCards ();
arrToCards (nameOfArr);
cards = document.querySelectorAll('.foto-conteiner');
hideSomeCards ();
}

function hideSomeCards () {
for (let i=12; i < cards.length; i++) {
    cards[i].style.display = "none";
}
}

function showMoreCardsAndHideBtn () {
    for (let i=12; i < (i+12); i++) {
        cards[i].style.display = "block";
        btnLoadMore.style.display = "none";
    } 
}

function arrToCards (arr) {
    arr.forEach(element => {
        photoBlocks.insertAdjacentHTML('beforeend', 
        `<div class="foto-conteiner">
            <img src=${element.img} alt="${element.name}">
            <span class="foto-description">
                <div class="circle-item">
                <a class="circle-item-p1" href="#">
                    <svg width="43" height="43" viewBox="0 0 43 43" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g clip-path="url(#clip0_2143_233)">
                    <rect x="1" y="2" width="41" height="40" rx="20" stroke="#18CFAB"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M26.9131 17.7282L25.0948 15.8913C24.2902 15.0809 22.983 15.0759 22.1768 15.8826L20.1592 17.8926C19.3516 18.6989 19.3482 20.0103 20.1505 20.8207L21.3035 19.689C21.1868 19.3284 21.3304 18.9153 21.6159 18.6295L22.8995 17.3519C23.3061 16.9462 23.9584 16.9491 24.3595 17.3543L25.4513 18.458C25.8528 18.8628 25.8511 19.5171 25.447 19.9232L24.1634 21.2024C23.8918 21.473 23.4461 21.6217 23.1002 21.5263L21.9709 22.6589C22.7745 23.4718 24.0803 23.4747 24.8889 22.6684L26.9039 20.6592C27.7141 19.8525 27.7167 18.5398 26.9131 17.7282ZM19.5261 25.0918C19.6219 25.4441 19.4686 25.8997 19.1909 26.1777L17.9923 27.3752C17.5807 27.7845 16.916 27.7833 16.5067 27.369L15.393 26.2475C14.9847 25.8349 14.9873 25.1633 15.3982 24.7547L16.598 23.5577C16.8903 23.2661 17.3104 23.1202 17.6771 23.2438L18.8335 22.0715C18.0149 21.2462 16.6825 21.2421 15.8606 22.0632L13.9152 24.0042C13.0923 24.8266 13.0884 26.1629 13.9065 26.9886L15.7582 28.8618C16.576 29.6846 17.9072 29.6912 18.7311 28.8701L20.6765 26.9287C21.4985 26.1054 21.5024 24.7717 20.6855 23.9443L19.5261 25.0918ZM19.2579 24.5631C18.9801 24.8419 18.5343 24.8411 18.2618 24.5581C17.9879 24.2743 17.9901 23.8204 18.2661 23.5399L21.5907 20.1611C21.8668 19.8823 22.3117 19.8831 22.5851 20.164C22.8605 20.4457 22.8588 20.9009 22.5817 21.183L19.2579 24.5631Z" fill="#18CFAB"/>
                    </g>
                    </svg>
                </a>
                <a class="circle-item-p2" href="#">
                </a>
                </div>
                <h1>${element.text}</h1>
                <p>${element.name}</p>
            </span>
        </div>`)
    });
}

function delleteCards () {
    cards.forEach(card => 
    card.remove())
};

let btnLoadMore = document.querySelector('.bnt.turquoise.load-more');
let btnLoadMoreText = document.querySelector('.textCross');
let btnLoadMoreLoader = document.querySelector('.loader');

btnLoadMore.addEventListener('click', loadMore)

function loadMore (e) {
btnLoadMoreText.style.display = "none";
btnLoadMoreLoader.style.display = "block";
let timer = setTimeout(showMoreCardsAndHideBtn, 2000);

};


// MAIN ----- our-amazing-work ------

let scrollBtnLeft = document.getElementsByClassName("pscroll_left");
let scrollBtnRight = document.getElementsByClassName("pscroll_right");
let person = document.getElementsByClassName("person");
let smallPhoto = document.querySelectorAll(".small-photo");

let index = 1;
showPerson (index);

function plusPerson(n) {
    showPerson(index += n);
}
  
function currentPerson(n) {
    showPerson(index = n);
}

function showPerson (n) {
    let i; 
    if (n>person.length) {index = 1}
    if (n<1) {index = person.length}
    for (i=0; i < person.length; i++) {
        
        person[i].classList.remove('active');
    }
    for (i=0; i < smallPhoto.length; i++) {
        smallPhoto[i].classList.remove('active');
    }

    person[index-1].classList.add('active');
    smallPhoto[index-1].classList.add('active');
    
    let mainPhoto = document.querySelector(".main-photo img");
    setTimeout(() => 
    mainPhoto.setAttribute("src", `img/7.PeopleSay/peopleSay-${index}.png`), 200)
}


